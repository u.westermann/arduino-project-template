ARDUINO_SRC_DIR = /usr/share/arduino/hardware/arduino/cores/arduino/
ARDUINO_SRC = wiring_analog.c wiring_digital.c wiring.c wiring_pulse.c WInterrupts.c wiring_shift.c Print.cpp WString.cpp Tone.cpp USBCore.cpp HardwareSerial.cpp WMath.cpp CDC.cpp Stream.cpp HID.cpp IPAddress.cpp new.cpp avr-libc/malloc.c avr-libc/realloc.c

SRCS = $(addprefix $(ARDUINO_SRC_DIR), $(ARDUINO_SRC)) main.cpp

BIN_DIR = bin
TMP_DIR = tmp
DOC_DIR = doc

TARGET = $(BIN_DIR)/main.hex

AS = avr-as
CC = avr-gcc
CXX = avr-g++
RM = rm -f
LINKER = avr-g++
OBJCOPY = avr-objcopy
SIZE = avr-size
FORMAT = clang-format -i -style=file
MKDIR = mkdir -p

OBJS = $(SRCS:%=$(TMP_DIR)/%.o)
DEPS = $(SRCS:%=$(TMP_DIR)/%.d)

MCU = atmega32u4
ASFLAGS =
CPPFLAGS = -MMD -I/usr/share/arduino/hardware/arduino/cores/arduino -I/usr/share/arduino/hardware/arduino/variants/micro -DF_CPU=16000000L -DUSB_VID=0x2341 -DUSB_PID=0x8037 -DARDUINO=105 -D__PROG_TYPES_COMPAT__
CFLAGS = -std=gnu11 -ggdb3 -Wall -Wextra -Wpedantic -fno-exceptions -ffunction-sections -fdata-sections -mmcu=$(MCU)
CXXFLAGS = -std=gnu++17 -ggdb3 -Wall -Wextra -Wpedantic -fno-exceptions -ffunction-sections -fdata-sections -mmcu=$(MCU)
LDFLAGS = -Wl,--gc-sections -mmcu=$(MCU)
LDLIBS =

ifdef release
	CFLAGS += -O$(release)
	CXXFLAGS += -O$(release)
else
	CFLGAS += -Og
	CXXFLAGS += -Og
endif

.PHONY: all
all: $(TARGET)

$(TARGET): $(OBJS)
	@echo LINKING: $<
	$(MKDIR) $(@D)
	$(LINKER) $(LDFLAGS) $(OBJS) $(LDLIBS) -o $(basename $@).elf
	$(OBJCOPY) -O ihex -R .eeprom $(basename $@).elf $@
	$(SIZE) --format=avr --mcu=$(MCU) $(basename $@).elf

$(TMP_DIR)/%.c.o: %.c
	@echo COMPILING: $<
	$(MKDIR) $(@D)
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

$(TMP_DIR)/%.cpp.o: %.cpp
	@echo COMPILING: $<
	$(MKDIR) $(@D)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@

$(TMP_DIR)/%.s.o: %.s
	@echo COMPILING: $<
	$(MKDIR) $(@D)
	$(AS) $(ASFLAGS) -c $< -o $@

.PHONY: program
program: all
	avrdude -p $(MCU) -c avrispmkII -U flash:w:$(TARGET)
	#./reset.py /dev/ttyACM0
	#avrdude -p $(MCU) -b 57600 -c avr109 -P /dev/ttyACM0 -U flash:w:$(TARGET)

.PHONY: clean
clean:
	@echo CLEANING
	$(RM) -r *.o *.d $(BIN_DIR) $(TMP_DIR) $(DOC_DIR) compile_commands.json tags

.PHONY: format
format:
	clang-format -i -style=file $(shell find . -iname '*.c' -or -iname '*.cpp' -or -iname '*.h')

.PHONY: check
check:
	@echo CPPCHECK
	cppcheck $(SRCS)
	@echo CLANG-TIDY
	clang-tidy $(SRCS)

.PHONY: doc
doc:
	@echo GENERATING DOCUMENTATION
	mkdir -p $(DOC_DIR)
	doxygen

.PHONY: tags
tags:
	@echo GENERATING TAGS
	ctags -R . $(ARDUINO_SRC_DIR)

.PHONY: bear
bear:
	make clean
	bear make

-include $(DEPS)
